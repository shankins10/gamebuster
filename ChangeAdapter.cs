﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class ChangeAdapter: IGetAmountInterface
    {
        private readonly IGetChangeInterface getChangeAmount;

        public ChangeAdapter(IGetChangeInterface getChangeAmount)
        {
            this.getChangeAmount = getChangeAmount;
        }

        public double GetAmount(double inputAmount, double priceOfItem)
        {
            if (inputAmount >= priceOfItem)
            {
                return getChangeAmount.GetChangeAmount(inputAmount, priceOfItem);
            }
            else
            {
                return getChangeAmount.GetNeededAmount(inputAmount, priceOfItem);
            }
        }
    }
}
