﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    /// <summary>
    /// Works as the database that stores data for the consoles and video games
    /// </summary>
    public class Items
    {
        public List<SaleItem> ItemList = new List<SaleItem>();

        public Items()
        {
            ItemList.Add(Xbox);
            ItemList.Add(PlayStation);
            ItemList.Add(GameCube);
        }

        public List<SaleItem> GetList()
        {
            return ItemList;
        }

        public SaleItem Xbox
        {
            get
            {
                var saleItem = SaleItem.GetSaleItem();
                saleItem.Name = "Xbox";
                saleItem.SellPrice = 150;
                return saleItem;
            }
        }

        public SaleItem PlayStation
        {
            get
            {
                var saleItem = SaleItem.GetSaleItem();
                saleItem.Name = "PlayStation";
                saleItem.SellPrice = 125;
                return saleItem;
            }
        }

        public SaleItem GameCube
        {
            get
            {
                var saleItem = SaleItem.GetSaleItem();
                saleItem.Name = "GameCube";
                saleItem.SellPrice = 75;
                return saleItem;
            }
        }
    }
}