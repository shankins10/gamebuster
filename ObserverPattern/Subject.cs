﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster.ObserverPattern
{
    public class Subject
    {
        private List<Observer> observers = new List<Observer>();
        private double sellPrice;

        public double GetSellPrice()
        {
            return sellPrice;
        }

        public void SetSellPrice(double sellPrice)
        {
            this.sellPrice = sellPrice;
            NotifyAllObservers();
        }

        public void Attach(Observer observer)
        {
            observers.Add(observer);
        }

        public void NotifyAllObservers()
        {
            foreach(Observer observer in observers)
            {
                observer.Update();
            }
        }
    }
}
