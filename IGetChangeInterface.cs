﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public interface IGetChangeInterface
    {
        public double GetChangeAmount(double inputAmount, double priceOfItem);

        public double GetNeededAmount(double inputAmount, double priceOfItem);
    }
}
