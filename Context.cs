﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class Context
    {
        private ITransactionMessage transactionMessage;

        public Context(ITransactionMessage transactionMessage)
        {
            this.transactionMessage = transactionMessage;
        }

        public string GetTransactionMessage()
        {
            return transactionMessage.GetMessage();
        }
    }
}
