﻿using GameBuster.ObserverPattern;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    class Program
    {
        static void Main(string[] args)
        {
            Subject subject = new Subject();
            SelectionController selectionController = new SelectionController();
            SellTransactionController transactionController = new SellTransactionController();
            Employee cashier = new Employee("", "cashier");
            Employee employee1 = new Employee("Anakin Skywalker", "cashier");
            Employee employee2 = new Employee("Luke Skywalker", "cashier");
            cashier.AddEmployee(employee1);
            cashier.AddEmployee(employee2);
            foreach(Employee employee in cashier.GetEmployees())
            {
                Console.WriteLine(employee.ToString());
            }

            Console.WriteLine("Press 1 to sell an item to customer, 2 to buy an item from customer, or 3 to change the price of an item");
            var purchasingChoice = selectionController.SelectOption(Console.ReadLine());

            Console.WriteLine("You have chosen to " + purchasingChoice + " item");
            var saleItem = selectionController.CreateSaleItem();
            if (purchasingChoice == "change price")
            {
                new SaleItem(subject);
                Console.WriteLine("What is the new sell price for the item?");
                subject.SetSellPrice(Convert.ToDouble(Console.ReadLine()));
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Which Item is being sold?");
                var itemName = Console.ReadLine();

                var priceOfItem = transactionController.GetSellPrice(saleItem, itemName);
                if (priceOfItem == 0)
                {
                    Console.WriteLine("Invalid input");
                    Console.ReadLine();
                }
                else
                {
                    //Console.WriteLine("The price for " + itemName + " will be $" + string.Format("{0:0.00}", priceOfItem));
                    Console.WriteLine(saleItem.ShowItemDetails());

                    Console.WriteLine("Please insert total");
                    var transactionAmount = Console.ReadLine();
                    Context context = new Context(new SellTransactionController());
                    try
                    {
                        var message = transactionController.MakeSellTransaction(Convert.ToDouble(transactionAmount), priceOfItem);
                        Console.WriteLine(message);
                        Console.ReadLine();
                    }
                    catch
                    {
                        Console.WriteLine("Invalid Input");
                        Console.ReadLine();
                    }
                }
            }
        }
    }
}