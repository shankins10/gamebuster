﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameBuster.ObserverPattern;

namespace GameBuster
{
    public class SaleItem : Observer, IItemDetails
    {
        public string Name { get; set; }

        public double SellPrice { get; set; }

        private static SaleItem saleItem = new SaleItem(null);

        public SaleItem(Subject subject)
        {
            if (subject != null)
            {
                this.subject = subject;
                this.subject.Attach(this);
            }
        }

        public static SaleItem GetSaleItem()
        {
            return saleItem;
        }

        public string ShowItemDetails()
        {
            return "The item is " + saleItem.Name + " with a selling price of $" + string.Format("{0:0.00}", saleItem.SellPrice);
        }

        public override void Update()
        {
            Console.WriteLine("The new Sell Price is " + subject.GetSellPrice());
        }
    }
}
