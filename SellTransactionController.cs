﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class SellTransactionController: ITransactionMessage
    {
        double _inputAmount;
        double _priceOfItem;
        double _changeAmount;

        public double GetSellPrice(SaleItem saleItem, string itemName)
        {
            var items = new Items();
            return itemName switch
            {
                "Xbox" => saleItem.SellPrice = items.Xbox.SellPrice,
                "PlayStation" => saleItem.SellPrice = items.PlayStation.SellPrice,
                "GameCube" => saleItem.SellPrice = items.GameCube.SellPrice,
                _ => 0,
            };
        }

        public string GetMessage()
        {
            if (_inputAmount == _priceOfItem)
            {
                return "Transaction is Successful!";
            }
            else if (_inputAmount >= _priceOfItem)
            {
                return "Please give customer $" + string.Format("{0:0.00}", _changeAmount) + " in change";
            }
            else if (_inputAmount <= _priceOfItem)
            {
                return "$" + string.Format("{0:0.00}", _changeAmount) + " is still needed from customer";
            }
            else
            {
                return "Invalid amount";
            }
        }

        public string MakeSellTransaction(double inputAmount, double priceOfItem)
        {
            _inputAmount = inputAmount;
            _priceOfItem = priceOfItem;
            var change = new GetChange();
            var changeAdapter = new ChangeAdapter(change);
            _changeAmount = changeAdapter.GetAmount(inputAmount, priceOfItem);
            return GetMessage();
        }
    }
}