﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class ChoiceFacade
    {
        private IGetChoice buyItem;
        private IGetChoice sellItem;
        private IGetChoice changePrice; 
        
        public ChoiceFacade()
        {
            buyItem = new BuyItem();
            sellItem = new SellItem();
            changePrice = new ChangePrice();
        }

        public string GetBuyChoice()
        {
           return buyItem.Choice();
        }

        public string GetSellChoice()
        {
            return sellItem.Choice();
        }

        public string GetChangePriceChoice()
        {
            return changePrice.Choice();
        }
    }
}
