﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class Employee
    {
        private string name;
        private string role;
        public List<Employee> employees;

        public Employee(string name, string role)
        {
            this.name = name;
            this.role = role;
            employees = new List<Employee>();
        }

        public void AddEmployee(Employee e)
        {
            employees.Add(e);
        }

        public List<Employee> GetEmployees()
        {
            return employees;
        }

        public string ToString()
        {
            return "Employee " + name + " is a " + role;
        }
    }
}
