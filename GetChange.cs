﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class GetChange: IGetChangeInterface
    {
        public double GetChangeAmount(double inputAmount, double priceOfItem)
        {
            return Math.Round(inputAmount - priceOfItem, 2);
        }

        public double GetNeededAmount(double inputAmount, double priceOfItem)
        {
            return Math.Round(priceOfItem - inputAmount, 2);
        }
    }
}
