﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GameBuster
{
    public class SelectionController : ITransactionMessage
    {
        private SaleItem SaleItem { get; }

        public string SelectOption(string selected)
        {
            ChoiceFactory purchasingChoice = new ChoiceFactory();

            if (selected == "1" || selected == "2" || selected == "3")
            {
                return purchasingChoice.GetChoice(selected);
            }
            else
            {
                return GetMessage();
            }
        }

        public string GetMessage()
        {
            return "Invalid input";
        }

        public SaleItem CreateSaleItem()
        {
            return SaleItem.GetSaleItem();
        }
    }
}
