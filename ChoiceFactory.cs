﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBuster
{
    public class ChoiceFactory
    {
        public string GetChoice(string selected)
        {
            var choice = new ChoiceFacade();
            if (selected == "1")
            {
                return choice.GetSellChoice();
            }
            else if (selected == "2")
            {
                return choice.GetBuyChoice();
            }
            else
            {
                return choice.GetChangePriceChoice();
            }
        }
    }
}
